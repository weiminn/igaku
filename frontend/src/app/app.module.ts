import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

// Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AgmCoreModule } from '@agm/core';
import { Router, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { ClinicDetailsComponent } from './clinic-details/clinic-details.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { AuthService } from './auth/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountsettingsComponent } from './accountsettings/accountsettings.component';
import { AccountSettingsModule } from './accountsettings/accountsettings.module';
import { SwiperModule } from 'ngx-useful-swiper';
// import pagination component
import { JwPaginationComponent } from 'jw-angular-pagination';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SplashComponent } from './splash/splash.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyAP4dY9zX5hRZzrSfdYymMtKsslkOelK-E',
  authDomain: 'clinigo-b8aee.firebaseapp.com',
  databaseURL: 'https://clinigo-b8aee.firebaseio.com',
  projectId: 'clinigo-b8aee',
  storageBucket: 'clinigo-b8aee.appspot.com',
  messagingSenderId: '872100690744'
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ClinicDetailsComponent,
    ContactUsComponent,
    AboutUsComponent,
    LoginComponent,
    RegisterComponent,
    ForgetPasswordComponent,
    JwPaginationComponent,
    SplashComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig), AngularFirestoreModule, AngularFireAuthModule, 
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCpVhQiwAllg1RAFaxMWSpQruuGARy0Y1k',
      libraries: ['places'],
    }),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AccountSettingsModule,
    SwiperModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiaWFtenRpciIsImEiOiJjamlsdXBqa3UwMTFsM2tud3AxbmNwMzBrIn0.0jZ5ls7sAcCW2fWBs6g9Eg', // Can also be set per map (accessToken input of mgl-map)
      geocoderAccessToken: 'sk.eyJ1IjoiaWFtenRpciIsImEiOiJjamlvbzZndm8wcG4wM3BwYTF1Y2Jya2huIn0.xFfDUIkSNerBZ7zEpHH1Aw' // Optionnal, specify if different from the map access token, can also be set per mgl-geocoder (accessToken input of mgl-geocoder)
    }),
    AngularFontAwesomeModule
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
