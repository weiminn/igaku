import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-account-clinic',
  templateUrl: './account-clinic.component.html',
  styleUrls: ['./account-clinic.component.css']
})
export class AccountClinicComponent implements OnInit {

  id;
  clinic;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _firestore: AngularFirestore,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params.id;
        this._firestore.collection('clinics').doc(this.id).snapshotChanges().subscribe(res => {
          console.log(res.payload.data());
          this.clinic = res.payload.data();
        });
      }
    )
  }
}
