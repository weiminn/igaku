import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-clinic-list-item',
  templateUrl: './clinic-list-item.component.html',
  styleUrls: ['./clinic-list-item.component.css']
})
export class ClinicListItemComponent implements OnInit {

  @Input() clinic;

  constructor() { }

  ngOnInit() {
  }

}
