import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


// Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  title = 'app';

  allClinics = [];
  
  insurances = [];
  selectedInsurances = [];

  categories = [];
  selectedCategories = [];

  noInsuranceBoxChecked = true;

  currentLocation: location = {
    ready: false,
  };

  constructor(private _firestore: AngularFirestore,
    private router: Router
  ) {
  }

  config: any = {
    slidesPerView: 1,
    slidesPerColumn: 5,
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    }
  };

  ngOnInit(){
    
    this.locateMe();
    
    this._firestore.collection('clinics').snapshotChanges().subscribe((res) => {
      
      for (let el of res){
        let toPush = {};
        
        toPush = el.payload.doc.data();
        toPush['id'] = el.payload.doc.id;

        this.allClinics.push(toPush);
      }

      console.log('All clinics: ');
      console.log(this.allClinics);
      this.getUniqueInsurances();
      this.getUniqueCategories();
    });
  }

  locateMe(){
    console.log('Retrieving location...');
    
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition
      ((position) => {
        console.log('Retrieved!');
        this.currentLocation =  {
          ready: true,
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          zoom: 15
        }
        console.log(this.currentLocation);
      });
    }
    else {
      console.log('Cannot retrieved!');
      this.currentLocation =  {
        ready: true,
        zoom: 11,
        lat: 1.355880,
        lng: 103.817509
      }
    }
  }

  getUniqueInsurances() {
    var allInsurances = [];
    for (let clinic of this.allClinics){
      for (let insurance of clinic.Insurance){
        allInsurances.push(insurance);
      }
    }
    this.selectedInsurances = this.insurances = Array.from(new Set(allInsurances));
    console.log('All Insurances: ');
    console.log(this.insurances);
  }

  getUniqueCategories(){
    var allCategories = [];
    for (let clinic of this.allClinics){
      allCategories.push(clinic.Category);
    }
    this.selectedCategories = this.categories = Array.from(new Set(allCategories));
    console.log('All Categories: ');
    console.log(this.categories.length);
  }

  checkboxChanged(returnValue, insurance) {
    if(returnValue === true) {
      this.selectedInsurances.push(insurance)
    }
    else {
      this.selectedInsurances = this.selectedInsurances.filter( x => x !== insurance);
    }
    console.log('Return Value: ');
    console.log(returnValue);

    console.log('Selected Insurances: ');
    console.log(this.selectedInsurances.length);
  }

  noInsuranceCheckboxChanged(returnValue){
    if(returnValue === true) {
      this.noInsuranceBoxChecked = true;
    }
    else {
      this.noInsuranceBoxChecked = false;
    }
  }

  categoryCheckboxChanged(returnValue, category) {
    if(returnValue === true) {
      this.selectedCategories.push(category)
    }
    else {
      this.selectedCategories = this.selectedCategories.filter( x => x !== category);
    }
    console.log('Category event: ');
    console.log(returnValue, category);

    console.log('Selected Categories: ');
    console.log(this.selectedCategories);
  }

  isSelected(insArr){
    var isSelected = false;
    
    if(insArr.length !== 0){
      for(let ins of insArr){
        for (let insurance of this.selectedInsurances){
          if (ins === insurance){
            isSelected = true;
          }
        }
      }
    }

    else {
      if(this.noInsuranceBoxChecked){
        isSelected = true;
      }
    }
    
    return isSelected;
  }

  isCategorySelected(category){
    if (this.selectedCategories.includes(category)){
      return true;
    }
    return false;
  }

  getIcon(category){
    var url = 'assets/images/MapIcons/' + category + '.png';
    return url
  }

  markerClicked(clinicId){
    console.log(clinicId);
    this.router.navigate(['clinic-details'], {queryParams: {id: clinicId}});
  }

  debug(event) {
    console.log(event);
  }

}

interface location {
  ready: boolean,
  zoom?: number,
  lat?: number,
  lng?: number
}

