var NodeGeocoder = require('node-geocoder');
const admin = require('firebase-admin');
const serviceAccount = require('./clinigo-b8aee-firebase-adminsdk-3d8ef-1387b1beac.json');
admin.initializeApp({
    credential : admin.credential.cert(serviceAccount)
});

const clinics = require('./Clinic data');

const collection = admin.firestore().collection('clinics');

var options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyAP4dY9zX5hRZzrSfdYymMtKsslkOelK-E', // for Mapquest, OpenCage, Google Premier
  formatter: null         // 'gpx', 'string', ...
};
 
var geocoder = NodeGeocoder(options);

// console.log(clinics);
for(let clinic of clinics){
  geocoder.geocode(clinic.Address).then((res) => {
    console.log(res[0].latitude, res[0].longitude);
    var lat = res[0].latitude; var long = res[0].longitude;
    console.log(clinic);
    clinic.latitude = lat;
    clinic.longitude = long;
    collection.add(clinic);
  });

  clinic["Doctor(s)/practitioner"] = [];
  clinic["Insurance"] = [];
  clinic["Specialties"] = [];
  clinic["Admins"] = [];
}

console.log(clinics.length);