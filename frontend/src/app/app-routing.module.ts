import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ClinicDetailsComponent } from './clinic-details/clinic-details.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AccountsettingsComponent } from './accountsettings/accountsettings.component';
import { AuthGuard } from './auth/auth-guard.service';
import { SplashComponent } from './splash/splash.component';

const routes: Routes = [
  {
    path: "",
    component: SplashComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "clinic-details",
    component: ClinicDetailsComponent
  },
  {
    path: "contact-us",
    component: ContactUsComponent
  },
  {
    path: "about-us",
    component: AboutUsComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "accountsettings",
    loadChildren: './accountsettings/accountsettings.module#AccountSettingsModule', 
    canActivate: [AuthGuard]
  },
  {path: '', redirectTo: 'home', pathMatch: 'full'},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ AuthGuard ]
})
export class AppRoutingModule { }