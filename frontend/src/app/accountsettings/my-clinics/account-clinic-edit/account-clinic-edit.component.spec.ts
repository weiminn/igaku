import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountClinicEditComponent } from './account-clinic-edit.component';

describe('AccountClinicEditComponent', () => {
  let component: AccountClinicEditComponent;
  let fixture: ComponentFixture<AccountClinicEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountClinicEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountClinicEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
