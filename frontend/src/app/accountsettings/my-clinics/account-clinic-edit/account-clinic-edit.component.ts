import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { FormGroup, FormControl, FormArray, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-account-clinic-edit',
  templateUrl: './account-clinic-edit.component.html',
  styleUrls: ['./account-clinic-edit.component.css']
})
export class AccountClinicEditComponent implements OnInit {

  id;
  clinic;
  editMode = false;
  clinicForm: FormGroup;
  

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _firestore: AngularFirestore,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = params.id;
        this.editMode = params['id'] != null;
        console.log(this.editMode);
        this.initForm();
      }
    )
  }

  onSubmit() {
    const newClinic: Clinic = {
      'Clinic Name': this.clinicForm.value['name'],
        
      'Category': this.clinicForm.value['category'],
        
      'Specialties':  this.clinicForm.value['specialties'],
    
      'Minimum Fee per session': this.clinicForm.value['minFee'],
      'Maximum Fee per session': this.clinicForm.value['maxFee'],
    
      'Insurance': this.clinicForm.value['insurances'],
    
      'Address': this.clinicForm.value['address'],
    
      'Hours': {    
        'Monday': this.clinicForm.value['monday'],
        'Tuesday': this.clinicForm.value['tuesday'],
        'Wednesday': this.clinicForm.value['wednesday'],
        'Thursday': this.clinicForm.value['thursday'],
        'Friday': this.clinicForm.value['friday'],
        'Saturday': this.clinicForm.value['saturday'],
        'Sunday': this.clinicForm.value['sunday'],
      },

      'Doctor(s)/practitioner': this.clinicForm.value['doctors'],

      'latitude': 0,
      'longitude': 0, 

      'Admins': this.clinicForm.value['admins']
    };

    this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + 
    newClinic.Address + '&key=AIzaSyAP4dY9zX5hRZzrSfdYymMtKsslkOelK-E').subscribe((res: any) => {
      console.log(res.results[0].geometry.location);
      newClinic.latitude = res.results[0].geometry.location.lat; 
      newClinic.longitude = res.results[0].geometry.location.lng;
      console.log(newClinic);
    });

    if (this.editMode) {
      this._firestore.collection('clinics').doc(this.id).set(newClinic).then((res) => {
        console.log(res);
      });
    } else {
      newClinic.Admins.push({id: localStorage.getItem('user')});
      this._firestore.collection('clinics').add(newClinic).then((res) => {
        
        console.log(res);
      });
    }

    console.log(this.clinicForm);

    // this.onCancel();
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private initForm() {
    let clinicName = '';
    let address = '';
    let category = '';
    let maxFee = '';
    let minFee = '';
    let monday = '';
    let tuesday = '';
    let wednesday = '';
    let thursday = '';
    let friday = '';
    let saturday = '';
    let sunday = '';

    let doctors = new FormArray([]);
    let insurances = new FormArray([]);
    let specialties = new FormArray([]);
    let admins = new FormArray([]);

    if (this.editMode) {

      this._firestore.collection('clinics').doc(this.id).snapshotChanges().subscribe(res => {
        console.log(res.payload.data());
        this.clinic = res.payload.data();

        clinicName = this.clinic['Clinic Name'];
        category = this.clinic['Category'];
        address = this.clinic['Address'];
        maxFee = this.clinic['Maximum Fee per session'];
        minFee = this.clinic['Minimum Fee per session'];
        monday = this.clinic['Hours'].Monday;
        tuesday = this.clinic['Hours'].Tuesday;
        wednesday = this.clinic['Hours'].Wednesday;
        thursday = this.clinic['Hours'].Thursday;
        friday = this.clinic['Hours'].Friday;
        saturday = this.clinic['Hours'].Saturday;
        sunday = this.clinic['Hours'].Sunday;

        this.clinicForm = new FormGroup({
          'name': new FormControl(clinicName, Validators.required),
          'category': new FormControl(category, Validators.required),
          'address': new FormControl(address, Validators.required),
          'maxFee': new FormControl(maxFee, Validators.required),
          'minFee': new FormControl(minFee, Validators.required),
          'doctors': doctors,
          'insurances': insurances,
          'specialties': specialties,
          'monday': new FormControl(monday, Validators.required),
          'tuesday': new FormControl(tuesday, Validators.required),
          'wednesday': new FormControl(wednesday, Validators.required),
          'thursday': new FormControl(thursday, Validators.required),
          'friday': new FormControl(friday, Validators.required),
          'saturday': new FormControl(saturday, Validators.required),
          'sunday': new FormControl(sunday, Validators.required),
          'admins': admins
        });

        console.log(this.clinicForm);

        if (this.clinic['Doctor(s)/practitioner']) {
          for (const doctor of this.clinic['Doctor(s)/practitioner']) {
            doctors.push(
              new FormGroup({
                'name': new FormControl(doctor.name),
              })
            );
          }
        }

        if (this.clinic['Insurance']) {
          for (const insurance of this.clinic['Insurance']) {
            insurances.push(
              new FormGroup({
                'name': new FormControl(insurance.name),
              })
            );
          }
        }

        if (this.clinic['Specialties']) {
          console.log(this.clinic['Specialties'])
          for (const speciality of this.clinic['Specialties']) {
            specialties.push(
              new FormGroup({
                'name': new FormControl(speciality.name),
              })
            );
          }
        }

        if (this.clinic['Admins']) {
          console.log(this.clinic['Admins'])
          for (const admin of this.clinic['Admins']) {
            admins.push(
              new FormGroup({
                'name': new FormControl(admin.name),
              })
            );
          }
        }
      });
    }

    this.clinicForm = new FormGroup({
      'name': new FormControl(clinicName, Validators.required),
      'category': new FormControl(category, Validators.required),
      'address': new FormControl(address, Validators.required),
      'maxFee': new FormControl(maxFee, Validators.required),
      'minFee': new FormControl(minFee, Validators.required),
      'doctors': doctors,
      'insurances': insurances,
      'specialties': specialties,
      'monday': new FormControl(monday, Validators.required),
      'tuesday': new FormControl(tuesday, Validators.required),
      'wednesday': new FormControl(wednesday, Validators.required),
      'thursday': new FormControl(thursday, Validators.required),
      'friday': new FormControl(friday, Validators.required),
      'saturday': new FormControl(saturday, Validators.required),
      'sunday': new FormControl(sunday, Validators.required),
      'admins': admins
    })
    console.log(this.clinicForm);
  }

  onAdd(array) {
    (<FormArray>this.clinicForm.get(array)).push(
      new FormGroup({
        'name': new FormControl(null, [Validators.required])
      })
    )
  }

  onDelete(array, index) {
    (<FormArray>this.clinicForm.get(array)).removeAt(index);
  }

}

interface Clinic {
  'Clinic Name': string,
    
  'Category': string,
    
  'Specialties':  any[];

  'Minimum Fee per session': string,
  'Maximum Fee per session': string,

  'Insurance': any[],

  'Address': string,

  'Hours': {},
  'Doctor(s)/practitioner': any[],

  'latitude': number,
  'longitude': number,

  'Admins': any[]
}