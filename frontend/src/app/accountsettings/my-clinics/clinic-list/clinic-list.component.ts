import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-clinic-list',
  templateUrl: './clinic-list.component.html',
  styleUrls: ['./clinic-list.component.css']
})
export class ClinicListComponent implements OnInit {

  clinics = [];
  

  constructor(
    private _firestore: AngularFirestore,
    ) { }

  ngOnInit() {
    this._firestore.collection('clinics').snapshotChanges().subscribe((res) => {
      for (let el of res){
        const adminList: any[] = el.payload.doc.data()['Admins'];

        if(
          this.containsAdmin(adminList)
        ){
          let toPush = {};
        
          toPush = el.payload.doc.data();
          toPush['id'] = el.payload.doc.id;

          this.clinics.push(toPush);
        }
      }
    })
  }

  containsAdmin(adminList){
    var contains = false;
    for (let admin of adminList){
      if (admin.id == localStorage.getItem('user')){
        contains = true;
      }
    }

    return contains;
  }
}
