import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { FormGroup, FormControl, FormArray, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userForm: FormGroup;
  user;

  constructor(
    private _firestore: AngularFirestore,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  onSubmit(){
    const newData = {
      fullName: '',
      email: '',
      phone: '',
    };

    newData.fullName = this.userForm.value['fullName'];
    newData.email = this.userForm.value['email'];
    newData.phone = this.userForm.value['phone'];

    this._firestore.collection('users').doc(localStorage.getItem('user')).set(newData).then((res) => {
    });
  }

  private initForm() {
    let fullName = '';
    let email = '';
    let phone = '';

    this._firestore.collection('users').doc(localStorage.getItem('user')).snapshotChanges().subscribe(res => {

      this.user = res.payload.data();

      fullName = this.user['FullName'];
      email = this.user['Email'];
      phone = this.user['Phone'];

      this.userForm = new FormGroup({
        'fullName': new FormControl(fullName, Validators.required),
        'email': new FormControl(email, Validators.required),
        'phone': new FormControl(phone, Validators.required)
      });
    });
  }

}
