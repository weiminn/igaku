import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountClinicComponent } from './account-clinic.component';

describe('AccountClinicComponent', () => {
  let component: AccountClinicComponent;
  let fixture: ComponentFixture<AccountClinicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountClinicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountClinicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
