import * as firebase from 'firebase';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()

export class AuthService {

    token: string;

    constructor(private router: Router,
        private _firestore: AngularFirestore,) {}

    signUpUser(email, password, newData) {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
            this._firestore.collection('users').add(newData).then((res) => {
                console.log(res)
                this.signInUser(email, password);
            });
        }).catch(error => console.log(error));
    }

    signInUser(email, password) {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(
            response => {
                this.router.navigate(['/']);
                firebase.auth().currentUser.getToken()
                .then(
                    token => {
                        this.token = token;
                        localStorage.setItem("user", firebase.auth().currentUser.uid);
                        localStorage.setItem("token", token);
                    }
                )
            }
        )
        .catch(
            error => console.log(error)
        );
    }

    getToken() {
        firebase.auth().currentUser.getIdToken().then(
            token => {
                this.token = token
            }
        );

        return this.token;
    }

    isAuthenticated() {
        return localStorage.getItem('token') != null;
        // return this.token != null;
    }

    logout() {
        firebase.auth().signOut();
        console.log('user ' + localStorage.getItem('user') + 'signed out!');
        localStorage.removeItem('token');
        this.token = null;
        this.router.navigate(['/']);
    }
}
