import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../auth.service';

import { AngularFirestore } from 'angularfire2/firestore';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthService,
    private _firestore: AngularFirestore,) {

  }

  ngOnInit() {
  }

  onRegister(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    const newData = {
      FullName: form.value.fullName,
      Email: form.value.email,
      Phone: form.value.password,
    };

    this.authService.signUpUser(email, password, newData);

  }
}
