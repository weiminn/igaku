import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-clinic-details',
  templateUrl: './clinic-details.component.html',
  styleUrls: ['./clinic-details.component.css']
})
export class ClinicDetailsComponent implements OnInit {
  id: string;
  clinic;
  sub: Subscription;

  constructor(private route: ActivatedRoute, private _firestore: AngularFirestore) { }

  ngOnInit() {
    this.retrieveId();
    this.retrieveClinic();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  retrieveId(){
    this.sub = this.route.queryParams
      .subscribe(params => {
        this.id = params.id;
        console.log(this.id); // popular
      });
  }

  retrieveClinic(){
    this._firestore.collection('clinics').doc(this.id).snapshotChanges().subscribe(res => {
      console.log(res.payload.data());
      this.clinic = res.payload.data();
    })
  }
}
