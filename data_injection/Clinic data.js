// JavaScript Object Notation

module.exports = [
  { 
    'ClinicName': 'Fernhill Psychology and Counselling',

    'Category': 'Psychologist',
        
    'Specialties': 
    [
      'Psychology and Counselling',
      'Pre and Post-Natal Support',
      'Clinical Supervision',
      'Crisis Counselling',
      'Coaching',
      'Mediation',
    ],

    'Minimum Fee per session': 150,
    'Maximum Fee per session': 200,

    'Insurance': [],

    'Address': '27 Woking Rd, 138705 Singapore',

    "Hours": 
    {
      'Monday': '8AM–11:30PM',
    'Tuesday': '8AM–11:30PM',
    'Wednesday': '8AM–11:30PM',
    'Thursday': '8AM–11:30PM',
    'Friday': '10AM–6PM',
    'Saturday': '8AM–6PM',
    'Sunday': '1pm–6PM',
    },
    'Doctor(s)/practitioner': 
    [
      'Jillian Bromley Director, Counsellor BA, DipAppPsych, RN, RPN',
      'Beata Sabala-Maslowska Counsellor and Occupational Therapist MA (Counselling), DipDegree Speech Path, MOTPrac. OT',
      'Patricia Evans MA (Counselling), BA (Hons), PG Cert Coaching',
      'Lilian Ing BA Hons (Psychology), MA (Clinical Psychology), Pg Dip Adoption & Attachment, SFBT, SF Coaching',
      'Rachel Dasler Masters of Counselling, MSW (Applied) Hons, BA Soc Pol',
      'Marta Gilbert Ma Clin Psych Joanna Barlas BA (Hons) Experimental Psychology, MSc Forensic Psychology, Doctor of Clinical Psychology',
    ]
  },
  {
    'ClinicName': 'City Osteopathy and Physiotherapy',

    'Category': 'Physiotherapist',
        
    'Specialties': [
    'Osteopathy',
    'Physiotherapy',
    'Naturopathy',
    'Sports Massage Therapy',
    'Craniosacral Therapy',
    'Physical Therapy',
    'Sports Injury Rehabilitation',
    'Orthopaedic Prehabilitation and Rehabilitation',
    ],
    
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
    
    'Insurance': [],
    
    'Address': '1 Fifth Ave, 268802 Singapore',
    
    'Hours': 
    {
    'Monday': '8AM–7PM',
    'Tuesday': '8AM–7PM',
    'Wednesday': '8AM–7PM',
    'Thursday': '8AM–7PM',
    'Friday': '8AM–7PM',
    'Saturday': '8AM–1PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': [],

  },
  {
    'ClinicName': 'Psynaptica',

    'Category': 'Psychologist',
        
    'Specialties': [
    'ADHD',
    'ANXIETY',
    'ANGER MANAGEMENT',
    'BEHAVIORAL ADDICTIONS',
    'BIPOLAR DISORDER',
    'EATING DISORDER', 
    'DEPRESSION',
    'MARITAL & RELATIONSHIP ISSUES',
    'OPPOSITIONAL DEFIANT DISORDER',
    'OBSESSIVE COMPULSIVE DISORDER',
    'PAIN MANAGEMENT',
    'POST TRAUMATIC STRESS DISORDER',
    'PSYCHOSIS',
    'STRESS'
    ],
    
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
    
    'Insurance': [],
    
    'Address': '44 Rochester Park, 139248 Singapore',
    
    'Hours': 
    {
    'Monday': '8:30AM–7:30PM',
    'Tuesday': '8:30AM–5:30PM',
    'Wednesday': '8:30AM–7:30PM',
    'Thursday': '8:30AM–5:30PM',
    'Friday': '8:30AM–5:30PM',
    'Saturday': '8:30AM–12:30PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    ['Ms Shrimathi Swaminathan Clinical Psychologist B.A. (Psychology) M.A. M.Phil. (Clinical Psychology) Member, Singapore Psychological Society Member, Australian Psychological Society EMDR',
    'Dr. Emma Waddington Clinical Psychologist B.Sc (Psychology) (Hons.) M.Sc (Cognitive Behaviour Therapy) Ph.D (Psychology)',
    'Dr. Clare Henn-Haase Clinical Psychologist B.A. (Psychology) M.Ed. (Community Counseling) Psy.D. (Clinical Psychology) Licensed Clinical Psychologist (USA)',
    'Dr. Shian-Ling Keng Clinical Psychologist B. Sc. (Psychology and Biology) M.A. (Clinical Psychology) Ph.D. (Clinical Psychology)',
    ]
  },
  {
    'ClinicName': 'The Sole Clinic @ Bukit Timah',

    'Category': 'Podiatrist',
        
    'Specialties': [
    'Physiotherapy & Podiatry',
    ],
    
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
    
    'Insurance': [],
    
    'Address': 'Bukit Timah Shopping Centre, 170 Upper Bukit Timah Road #09-02, 588179 Singapore',
    
    'Hours': 
    {
    'Monday': '9AM–9PM',
    'Tuesday': '9AM–9PM',
    'Wednesday': '9AM–9PM',
    'Thursday': 'Closed',
    'Friday': '9AM–9PM',
    'Saturday': '9AM–6PM',
    'Sunday': 'Closed',
    },
   'Doctor(s)/practitioner': [],
  },
  {
   'ClinicName': 'OneDoctors Family Clinic (HV)',
    
   'Category': 'GP',
      
   'Specialties':  [
   'General checkup',
   ],
  
   'Minimum Fee per session': 'Mon-Fi 9AM-5PM 32 SGD per consultation 5PM-10PM 35 SGD per consultation | Sat-Sun 9AM-1PM 35 SGD per consultation 1PM-10PM 40 SGD per consultation',
   'Maximum Fee per session': 'none',

   'Insurance': ['CHAS'],
  
   'Address': '255 Holland Ave, 278983 Singapore',
  
   'Hours': 
   {
   'Monday': '9AM–10PM',
   'Tuesday': '9AM–10PM',
   'Wednesday': '9AM–10PM',
   'Thursday': '9AM–10PM',
   'Friday': '9AM–10PM',
   'Saturday': '9AM–10PM',
   'Sunday': '5–10PM',
   },

   'Doctor(s)/practitioner': 
   [
   'DR Boey Kok Hoe',
   'DR Norman Koh',
   'DR Ryan Wong',
   'DR Chung Wan Ling',
   ]
  },
  {
   'ClinicName': 'Oasis Family Clinic',
    
   'Category': 'GP',
      
   'Specialties':  [
   'Infectious Disease Treatment',
   'Health Checkup (General)',
   'Health screening',
   'Preventive Medicine',
   'Fever Treatment',
   'Acute Medical Conditions',
   'Vaccination/ Immunization',
   'Skin Check',
   'Acute & Chronic Medical Conditions',
   'Infectious Diseases',
   'Health check up',
   'General illness',
   'Health Screening',
   'Fever',
   'Vaccinations',
   'Chronic Medical Conditions',
   ],
  
   'Minimum Fee per session': 20,
   'Maximum Fee per session': 'none',
  
   'Insurance': ['AIA integrated health solutions'],
  
   'Address': '56 New Upper Changi Rd 461056 Singapore',
  
   'Hours': 
   {
    'Monday': '8AM–9:15PM',
    'Tuesday': '8AM–9:15PM',
    'Wednesday': '8AM–9:15PM',
    'Thursday': '8AM–9:15PM',
    'Friday': '9AM–9PM',
    'Saturday': '9AM–6PM',
    'Sunday': 'Closed',
   },
   'Doctor(s)/practitioner': 
   ['Dr. Chong Tze-Horng MBBS 18 years experience General Physician , Family Physician',
   'Dr. Wang Shi Tah MBBS General Physician , Family Physician',
   ]
  },
  {
   'ClinicName': 'Town Hall Clinic',
  
   'Category': 'GP',
      
   'Specialties': 'none',
   'Fee per session': 'none',
  
   'Insurance': ['AIA integrated health solutions',
   'CHAS',
   'AXA Group Clinicare Insurance',
   ],
  
   'Address': 'MacRitchie Nature Trail, Sin Ming 574623 Singapore',
   
  
   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Hours': 
   { 
    'Monday': '8:30AM–4:30PM',
    'Tuesday': '8:30AM–4:30PM',
    'Wednesday': '8:30AM–4:30PM',
    'Thursday': '8:30AM–4:30PM',
    'Friday': '8:30AM–4:30PM',
    'Saturday': 'none',
    'Sunday': 'none',
   },
   'Doctor(s)/practitioner': 
   ['Dr. Gwee Jin Hwa']
  },
  {
   'ClinicName': 'Aspen Clinic',
    
   'Category': 'GP',
      
   'Specialties':  [
   'Health Checkup (General)',
   'Vaccination/ Immunization',
   'Fever Treatment',
   'Childhood developmental assessment',
   'Acute medical treatment',
   'Chronic illness management',
   'pap Smear',
   'Acne / Pimples Treatment', 
   'chemical peeling',
   'Health Screening',
   'Health Checkup',
   'Child Vaccination',
   'Ear Syringing',
   'Removal of foreign body',
   'Nail Avulsion',
   'Minor surgical procedures, e.g. toilet and suture, incision and drainage', ],
  
   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',
  
   'Insurance': ['AIA integrated health solutions',
   'CHAS',
   'AXA Group Clinicare Insurance',],
  
   'Address': '283 Bishan Street 22 570283 Singapore',
  
   'Hours': 
   {
   'Monday': '9AM–12:30PM, 2–4:30PM, 7–9PM', 
   'Tuesday': '9AM–12:30PM, 2–4:30PM',
   'Wednesday': '9AM–12:30PM, 2–4:30PM, 7–9PM',
   'Thursday': '9AM–12:30PM, 2–4:30PM, 7–9PM',
   'Friday': '9AM–12:30PM, 2–4:30PM',
   'Saturday': '9AM–12:30PM',
   'Sunday': 'Closed',
   },
   'Doctor(s)/practitioner': 
   ['Dr. Wong Kin Chan MBBS, Graduate Diploma in Family Medicine, Singapore, Grad. Dip. Family Practice Dermatology 23 years experience General Physician']
  },
  {
   'ClinicName': 'Chung Kiaw Clinic',
    
   'Category': 'GP',
      
   'Specialties':  [
   'Management of chronic diseases', 
   'House calls for elderly and chronic sick',
   'Eldershield and IDAPE assessment',
   'Health screenings',
   'Vaccinations',],
  
   
  
   'Minimum Fee per session': 25,
   'Maximum Fee per session': 'none',
  
   'Insurance': ['AIA integrated health solutions',
   'CHAS',
   'AXA Group Clinicare Insurance',],
  
   'Address': '118 Upper Bukit Timah Rd 588173 Singapore',
  
   'Hours': 
   {
   'Monday': '9AM–12PM, 2–5PM',
   'Tuesday': '9AM–12PM, 2–4PM',
   'Wednesday': '9AM–12PM, 2–4PM',
   'Thursday': '9AM–12PM',
   'Friday': '9AM–12PM, 2–5PM',
   'Saturday': 'Closed',
   'Sunday': '9AM–12PM',
   },
   'Doctor(s)/practitioner': 
   ['Dr. Kelvin T. C. Koh',]
  },
  {
    'ClinicName': 'The Neptune Clinic',
        
    'Category': 'GP',
        
    'Specialties': [
     'Health Checkup (General)',
     'Vaccination/ Immunization',
     'Health screening',
     'skin checks',
     'Infectious Disease Treatment',
     'Fever Treatment',
     'Preventive Medicine',
     'Acute Medical Conditions',
     'Health check up',
     'Vaccinations',
     'Health Screening',
     'General illness',
     'Skin Checks',
     'Infectious Diseases',
     'Fever',
    ],
    
      'Minimum Fee per session': 20,
      'Maximum Fee per session': 'none',
    
     'Insurance': [],
    
     'Address': '328 Clementi Ave 2, 120328 Singapore',
    
     'Hours': 
     {
     'Monday': '08:00 AM-12:00 PM 05:00 PM-09:00 PM',
     'Tuesday': '08:00 AM-12:00 PM 05:00 PM-09:00 PM',
     'Wednesday': '08:00 AM-12:00 PM 05:00 PM-09:00 PM',
     'Thursday': '08:00 AM-12:00 PM 05:00 PM-09:00 PM',
     'Friday': '08:00 AM-12:00 PM',
     'Saturday': '08:00 AM-12:00 PM',
     'Sunday': 'Closed',
     },
    'Doctor(s)/practitioner': 
    ['Dr. Masayu Zainab Masagos Mohammed MBBS, Diploma in Family Medicine (DFM), Diploma in Geriatric Medicine 26 years experience General Physician , Family Physician',]
  },
  {'ClinicName': 'Choice Clinic Vista Point',
        
   'Category': 'GP',
      
   'Specialties': [
   
   ],
  
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
  
   'Insurance': [],
  
   'Address': '548 Woodlands Drive 44, #02-21, Singapore 730548',
  
   'Hours': 
   {
   'Monday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM',
   'Tuesday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM',
   'Wednesday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM',
   'Thursday': '08:30 AM–12:30 PM, 07:00 PM–09:00 PM',
   'Friday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM',
   'Saturday': '08:30 AM-12:30 PM',
   'Sunday': '08:30 AM-12:30 PM',
   },
   'Doctor(s)/practitioner': 
    [],
  },
  {'ClinicName': 'Woodgrove Medical',
        
   'Category': 'GP',
      
   'Specialties': [
    'Health Screening',
    'Chronic Diseases (Medisave deductible)',
    'Child Health',
    'Child Vaccinations',
    'Skin Diseases',
    'Mens Health',
    '​Family and Internal Medicine',
    'Checkups, Work Permits'
   ],
  
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
  
   'Insurance': [
     'CHAS',
     ],
  
   'Address': '30 woodlands ave 1 #02-03/04 singapore 739065',
  
   'Hours': 
   {
   'Monday': '08:30 AM–04:00 PM, 06:00 PM–9:30 PM',
   'Tuesday': '08:30 AM–04:00 PM, 06:00 PM–9:30 PM',
   'Wednesday': '08:30 AM–04:00 PM, 06:00 PM–9:30 PM',
   'Thursday': '08:30 AM–04:00 PM, 06:00 PM–9:30 PM',
   'Friday': '08:30 AM–04:00 PM, 06:00 PM–9:30 PM',
   'Saturday': '08:30 AM–04:00 PM',
   'Sunday': '08:30 AM–04:00 PM',
   },
   'Doctor(s)/practitioner': 
    [],
  },
  {'ClinicName': 'Woodlands Clinic',
        
   'Category': 'GP',
      
   'Specialties': [
    'General Practice/Family Medicine',
   
   ],
  
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
  
   'Insurance': [],
  
   'Address': '01-204, 131 Marsiling Rise, 730131',
  
   'Hours': 
   {
   'Monday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM, 06:30 PM–09:30 PM',
   'Tuesday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM, 06:30 PM–09:30 PM',
   'Wednesday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM, 06:30 PM–09:30 PM',
   'Thursday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM, 06:30 PM–09:30 PM',
   'Friday': '08:30 AM–12:30 PM, 02:00 PM–04:30 PM',
   'Saturday': '08:30 AM-12:30 PM',
   'Sunday': 'closed',
   },
   'Doctor(s)/practitioner': 
    [],
  },
  {'ClinicName': 'Healthway Medical Clinic',
        
    'Category': 'GP',
     
    'Specialties': [
    'General Practice/Family Medicine',
  
    ],
 
    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',
 
    'Insurance': [],
 
    'Address': 'Blk 888 Woodlands Drive 50 #02-737 888 Plaza, 730888',
 
    'Hours': 
    {
    'Monday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:30 PM–09:30 PM',
    'Tuesday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:30 PM–09:30 PM',
    'Wednesday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:30 PM–09:30 PM',
    'Thursday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:30 PM–09:30 PM',
    'Friday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM',
    'Saturday': '08:30 AM–01:00 PM',
    'Sunday': '08:30 AM-12:30 PM',
    },
    'Doctor(s)/practitioner': 
    [],
  },
  {
    'ClinicName': 'Central 24-HR Clinic (Jurong West)',
        
    'Category': 'GP',
   
    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '492 Jurong West Street 41, #01-54, 640492',

    'Hours':  {
      'Monday': 'Open 24 hours',
      'Tuesday': 'Open 24 hours',
      'Wednesday': 'Open 24 hours',
      'Thursday': 'Open 24 hours',
      'Friday': 'Open 24 hours',
      'Saturday': 'Open 24 hours',
      'Sunday': 'Open 24 hours',
    },
  
    'Doctor(s)/practitioner': 
    [],
  },
  {
    'ClinicName': 'Drs Koo, Fok & Associates (Pioneer) Pte Ltd',
        
    'Category': 'GP',
    
    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '31 Jurong West Street 63, #02-04, 648310',

    'Hours': 
    {
    'Monday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 05:30 PM–08:30 PM',
    'Tuesday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 05:30 PM–08:30 PM',
    'Wednesday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 05:30 PM–08:30 PM',
    'Thursday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 05:30 PM–08:30 PM',
    'Friday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 05:30 PM–08:30 PM',
    'Saturday': '08:30 AM-12:30 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {'ClinicName': 
    'Keat Hong Family Medicine Clinic',
        
    'Category': 'GP',
  
    'Specialties': [
    'General Practice/Family Medicine',

   ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': 'Keat Hong Community Club, 2 Choa Chu Kang Loop, #03-02, Singapore, 689687',

    'Hours': 
    {
    'Monday': '08:00 AM–07:30 PM',
    'Tuesday': '08:00 AM–05:00 PM',
    'Wednesday': '08:00 AM–05:00 PM',
    'Thursday': '08:00 AM–07:30 PM',
    'Friday': '08:00 AM–05:00 PM',
    'Saturday': '08:00 AM-12:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Clover Medical Clinic Pte Ltd',
        
    'Category': 'GP',
      
    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '21 Choa Chu Kang North 6, #B1-25 Yew Tee Point, 689578',

    'Hours': 
    {
    'Monday': '08:30 AM–11:45 AM, 01:00 PM–04:45 PM, 06:00 PM–09:15 PM',
    'Tuesday': '08:30 AM–11:45 AM, 01:00 PM–04:45 PM, 06:00 PM–09:15 PM',
    'Wednesday': '08:30 AM–11:45 AM, 01:00 PM–04:45 PM, 06:00 PM–09:15 PM',
    'Thursday': '09:00 AM–12:45 AM, 01:00 PM–04:45 PM, 06:00 PM–09:15 PM',
    'Friday': '09:00 AM–12:45 AM, 06:00 PM–08:45 PM',
    'Saturday': '09:00 AM-12:45 PM, 06:00 PM-8:45 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Havelock Family Clinic',
        
    'Category': 'GP',
 
    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '22 Havelock Rd, #01-709, Singapore 160022',

    'Hours': 
    {
    'Monday': '09:00 AM–04:00 PM, 07:00 PM-09:00 PM',
    'Tuesday': 'Closed',
    'Wednesday': '09:00 AM–04:00 PM',
    'Thursday': '09:00 AM–04:00 PM, 07:00 PM-09:00 PM',
    'Friday': '09:00 AM–04:00 PM',
    'Saturday': '09:00 AM-04:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Temasek Medical Clinic',
        
    'Category': 'GP',
  
    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '86 Bedok North Street 4',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM–04:00 PM, 07:00 PM–09:00 PM',
    'Tuesday': '08:30 AM–12:30 PM, 02:00 PM–04:00 PM, 07:00 PM–09:00 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM–04:00 PM, 07:00 PM–09:00 PM',
    'Thursday': '08:30 AM–12:30 PM, 02:00 PM–04:00 PM, 07:00 PM–09:00 PM',
    'Friday': '08:30 AM–12:30 PM',
    'Saturday': '08:30 AM–12:30 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Simei Clinic & Surgery',
        
    'Category': 'GP',
  
    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '248 Simei Street 5, #01-132, 520248',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM–05:00 PM, 07:00 PM–09:30 PM',
    'Tuesday': '08:30 AM–12:30 PM, 02:00 PM–05:00 PM, 07:00 PM–09:30 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM–05:00 PM, 07:00 PM–09:30 PM',
    'Thursday': '08:30 AM–12:30 PM, 02:00 PM–05:00 PM, 07:00 PM–09:30 PM',
    'Friday': '08:30 AM–12:30 PM, 02:00 PM–05:00 PM, 07:00 PM–09:30 PM',
    'Saturday': '08:30 AM–12:30 PM, 02:30 PM–05:00 PM',
    'Sunday': '07:00 PM-09:30 PM',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'ALLHEALTH FAMILY CLINIC',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': ' 872C TAMPINES ST86, #01-01, 523872',

    'Hours': 
    {
    'Monday': '09:00 AM–02:00 PM, 06:00 PM-09:00 PM',
    'Tuesday': '09:00 AM–02:00 PM, 06:00 PM-09:00 PM',
    'Wednesday': '09:00 AM–02:00 PM, 06:00 PM-09:00 PM',
    'Thursday': '09:00 AM–02:00 PM, 06:00 PM-09:00 PM',
    'Friday': '09:00 AM–02:00 PM',
    'Saturday': '09:00 AM–02:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Hamid Family Clinic & Surgery',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': '#01-35, 156 Pasir Ris Street 13, Block 156, Singapore 510156',

   'Hours': 
   {
   'Monday': '09:00 AM–12:00 PM, 01:30 PM–03:30 PM, 07:00 PM–09:00 PM',
   'Tuesday': '09:00 AM–12:00 PM, 01:30 PM–03:30 PM, 07:00 PM–09:00 PM',
   'Wednesday': '09:00 AM–12:00 PM, 01:30 PM–03:30 PM, 07:00 PM–09:00 PM',
   'Thursday': '09:00 AM–12:00 PM, 01:30 PM–03:30 PM, 07:00 PM–09:00 PM',
   'Friday': '09:00 AM–12:00 PM, 01:30 PM–03:30 PM, 07:00 PM–09:00 PM',
   'Saturday': '09:00 AM–12:00 PM',
   'Sunday': 'Closed',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 {
   'ClinicName': 'My Family Clinic (Anchorvale)',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': 'Blk 326A Anchorvale Road #01-260, 541326',

   'Hours': 
   {
   'Monday': '08:00 AM–02:00 PM, 06:00 PM-09:00 PM',
   'Tuesday': '09:00 AM–01:00 PM',
   'Wednesday': '08:00 AM–02:00 PM, 06:00 PM-09:00 PM',
   'Thursday': '08:00 AM–02:00 PM, 06:00 PM-09:00 PM',
   'Friday': '08:00 AM–02:00 PM',
   'Saturday': '08:00 AM–02:00 PM',
   'Sunday': 'Closed',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 {
   'ClinicName': 'AE Medical Clinic - Fernvale',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': '467B Fernvale Link, #01-529, 792467',

   'Hours': 
   {
   'Monday': '08:00 AM–12:00 PM, 06:00 PM-10:00 PM',
   'Tuesday': '08:00 AM–12:00 PM, 06:00 PM-10:00 PM',
   'Wednesday': '08:00 AM–12:00 PM, 06:00 PM-10:00 PM',
   'Thursday': '08:00 AM–12:00 PM, 06:00 PM-10:00 PM',
   'Friday': '08:00 AM–12:00 PM, 06:00 PM-10:00 PM',
   'Saturday': '08:30 AM–12:30 PM',
   'Sunday': '08:30 AM–12:30 PM',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 {
   'ClinicName': 'Yio Chu Kang MRT Clinic',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': '#01-5125, 569804, 635 Ang Mo Kio Ave 6, Block 635, 560635',

   'Hours': 
   {
   'Monday': '08:00 AM–12:15 PM, 02:00 PM–03:15 PM, 05:30 PM–08:15 PM',
   'Tuesday': '08:00 AM–12:15 PM, 02:00 PM–03:15 PM, 05:30 PM–08:15 PM',
   'Wednesday': '08:00 AM–12:00 PM',
   'Thursday': '08:00 AM–12:15 PM, 02:00 PM–03:15 PM, 05:30 PM–08:15 PM',
   'Friday': '08:00 AM–12:15 PM, 02:00 PM–03:15 PM, 05:30 PM–08:15 PM',
   'Saturday': '08:00 AM–12:00 PM',
   'Sunday': 'Closed',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 {
   'ClinicName': 'My Family Clinic (Punggol Waterway Terrace)',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': '01-302 Waterway Terraces 1, 308C Punggol Walk, Singapore 823308',

   'Hours': 
   {
   'Monday': '08:00 AM–02:00 PM, 06:00 PM-09:00 PM',
   'Tuesday': '09:00 AM–01:00 PM',
   'Wednesday': '08:00 AM–02:00 PM, 06:00 PM-09:00 PM',
   'Thursday': '08:00 AM–02:00 PM, 06:00 PM-09:00 PM',
   'Friday': '08:00 AM–02:00 PM',
   'Saturday': '08:00 AM–01:00 PM',
   'Sunday': 'Closed',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 {
   'ClinicName': 'Peace Family Clinic & Surgery',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': '406 Sembawang Dr, #01-820, Singapore 750406',

   'Hours': 
   {
   'Monday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:00 PM–09:00 PM',
   'Tuesday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:00 PM–09:00 PM',
   'Wednesday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:00 PM–09:00 PM',
   'Thursday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:00 PM–09:00 PM',
   'Friday': '08:30 AM–01:00 PM, 02:00 PM–05:00 PM, 06:00 PM–09:00 PM',
   'Saturday': '08:30 AM–01:00 PM',
   'Sunday': '08:30 AM–01:00 PM',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 { 
   'ClinicName': 'Wen & Weng Family Clinic',
        
   'Category': 'GP',

   'Specialties': [
   'General Practice/Family Medicine',

   ],

   'Minimum Fee per session': 'none',
   'Maximum Fee per session': 'none',

   'Insurance': [],

   'Address': 'Blk 505 Canberra Link, #01-03, 750505',

   'Hours': 
   {
   'Monday': '08:30 AM–12:00 PM, 02:00 PM–04:00 PM, 07:00 PM–08:30 PM',
   'Tuesday': '08:30 AM–12:00 PM, 02:00 PM–04:00 PM, 07:00 PM–08:30 PM',
   'Wednesday': '08:30 AM–12:00 PM, 02:00 PM–04:00 PM, 07:00 PM–08:30 PM',
   'Thursday': '08:30 AM–12:00 PM, 02:00 PM–04:00 PM, 07:00 PM–08:30 PM',
   'Friday': '08:30 AM–12:00 PM, 02:00 PM–04:00 PM, 07:00 PM–08:30 PM',
   'Saturday': '08:30 AM–12:00 PM',
   'Sunday': '08:30 AM–12:00 PM',
   },
   'Doctor(s)/practitioner': 
   [],
 },
 {
   'ClinicName': 'The Cliniq',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '#01-23, 590 Montreal Dr, 751590',

    'Hours': 
    {
    'Monday': '09:00 AM–12:00 PM, 04:00 PM–06:00 PM, 08:00 PM–11:00 PM',
    'Tuesday': '09:00 AM–12:00 PM, 04:00 PM–06:00 PM, 08:00 PM–11:00 PM',
    'Wednesday': '09:00 AM–12:00 PM, 04:00 PM–06:00 PM, 08:00 PM–11:00 PM',
    'Thursday': '09:00 AM–12:00 PM, 04:00 PM–06:00 PM, 08:00 PM–11:00 PM',
    'Friday': 'Closed',
    'Saturday': 'Closed',
    'Sunday': '09:00 AM–12:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Atrio Family Clinic Pte. Ltd.',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '291 Yishun Street 22, Singapore 760291',

    'Hours': 
    {
    'Monday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    'Tuesday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    'Wednesday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    'Thursday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    'Friday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    'Saturday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    'Sunday': '08:00 AM–02:00 PM, 05:30 PM-10:30 PM',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 { 
   'ClinicName': 'OneDoctors Family Clinic (Yishun)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '#01-3611, 846 Yishun Ring Rd, Singapore 760846',

    'Hours': 
    {
    'Monday': '08:00 AM–10:00 PM',
    'Tuesday': '08:00 AM–10:00 PM',
    'Wednesday': '08:00 AM–10:00 PM',
    'Thursday': '08:00 AM–10:00 PM',
    'Friday': '08:00 AM–10:00 PM',
    'Saturday': '08:00 AM–10:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
  'ClinicName': 
    'Regional Patientcare Medical Group Pte Ltd',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '#01-3611, 846 Yishun Ring Rd, Singapore 760846',

    'Hours': 
    {
    'Monday': '08:30 AM–03:45 PM',
    'Tuesday': '08:30 AM–03:45 PM',
    'Wednesday': '08:30 AM–03:45 PM',
    'Thursday': '08:30 AM–03:45 PM',
    'Friday': '08:30 AM–03:45 PM',
    'Saturday': '08:30 AM–12:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
 },
 {
   'ClinicName': 'Yishun Central Medical Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '160 Yishun Street 11, #01-212 Singapore 760160',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 06:30 PM-09:00 PM',
    'Tuesday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 06:30 PM-09:00 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 06:30 PM-09:00 PM',
    'Thursday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 06:30 PM-09:00 PM',
    'Friday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 06:30 PM-09:00 PM',
    'Saturday': 'Closed',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
    'ClinicName':'Greenlink Medical Clinic Pte Ltd',
          
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': 'Blk 807 Yishun Ring Road, #01-4213, 760807',

    'Hours': 
    {
    'Monday': '08:30 AM–02:30 PM, 06:30 PM-08:30 PM',
    'Tuesday': '08:30 AM–02:30 PM, 06:30 PM-08:30 PM',
    'Wednesday': '08:30 AM–02:30 PM, 06:30 PM-08:30 PM',
    'Thursday': '08:30 AM–02:30 PM, 06:30 PM-08:30 PM',
    'Friday': '08:30 AM–02:30 PM',
    'Saturday': '08:30 AM–12:30 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Island Family Clinic (Keat Hong)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': 'BLOCK 803 KEAT HONG CLOSE #01-04, 680803',

    'Hours': 
    {
    'Monday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Tuesday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Wednesday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Thursday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Friday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Saturday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Sunday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{   
   'ClinicName': 'NuHealth Medical Centre',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '1 Woodlands Road #01-23 Junction 10, 677899',

    'Hours': 
    {
    'Monday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Tuesday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Wednesday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Thursday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Friday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Saturday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Sunday': '08:30 AM–01:00 PM, 02:00 PM-04:30 PM, 06:30 PM-10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName': 'Lee & Koh Family Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '359 Bukit Batok Street 31, Singapore 650359',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 07:00 PM-09:00 PM',
    'Tuesday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 07:00 PM-09:00 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 07:00 PM-09:00 PM',
    'Thursday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 07:00 PM-09:00 PM',
    'Friday': '08:30 AM–12:30 PM, 02:00 PM-04:00 PM, 07:00 PM-09:00 PM',
    'Saturday': '08:45 AM–12:30 PM',
    'Sunday': '09:00 AM–12:15 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName': 'Prohealth Medical Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '259 Bukit Panjang Ring Road, Block 259, 671259',

    'Hours': 
    {
      'Monday': 'Open 24 hours',
      'Tuesday': 'Open 24 hours',
      'Wednesday': 'Open 24 hours',
      'Thursday': 'Open 24 hours',
      'Friday': 'Open 24 hours',
      'Saturday': 'Open 24 hours',
      'Sunday': 'Open 24 hours',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Sunbeam Medical Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '443c Fajar Rd #01-76, 673443',

    'Hours': 
    {
    'Monday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Tuesday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Wednesday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Thursday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Friday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Saturday': '08:30 AM–01:00 PM',
    'Sunday': '08:30 AM–01:00 PM, 06:00 PM-10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Pioneer Medicare',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '3 Soon Lee Rd, Singapore 628070',

    'Hours': 
    {
    'Monday': '08:30 AM–04:00 PM',
    'Tuesday': '08:30 AM–04:00 PM',
    'Wednesday': '08:30 AM–04:00 PM',
    'Thursday': '08:30 AM–04:00 PM',
    'Friday': '08:30 AM–04:00 PM',
    'Saturday': '09:00 PM-12:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'West Point Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '853 Jurong West Street 81, Singapore 640853',

    'Hours': 
    {
    'Monday': '09:00 AM–11:45 PM',
    'Tuesday': '09:00 AM–11:45 PM',
    'Wednesday': '09:00 AM–11:45 PM',
    'Thursday': '09:00 AM–11:45 PM',
    'Friday': '09:00 AM–11:45 PM',
    'Saturday': '09:00 AM–11:45 PM',
    'Sunday': '09:00 AM–11:45 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'My Family Clinic (Pioneer)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '638 Jurong West Street 61, Singapore 640638',

    'Hours': 
    {
    'Monday': '08:00 AM–09:00 PM',
    'Tuesday': '08:00 AM–09:00 PM',
    'Wednesday': '08:00 AM–09:00 PM',
    'Thursday': '08:00 AM–09:00 PM',
    'Friday': '08:00 AM–09:00 PM',
    'Saturday': '08:00 AM–09:00 PM',
    'Sunday': '08:00 AM–01:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Pioneer Medical Centre Pte Ltd.',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',

    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '651 Jurong West Street 61 #01-06, 640651',

    'Hours': 
    {
    'Monday': '08:30 AM–02:30 PM, 06:30 PM-10:00 PM',
    'Tuesday': '08:30 AM–02:30 PM, 06:30 PM-10:00 PM',
    'Wednesday': '08:30 AM–02:30 PM, 06:30 PM-10:00 PM',
    'Thursday': '08:30 AM–12:00 PM, 02:30 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Friday': '08:30 AM–12:00 PM, 02:30 PM-04:30 PM, 06:30 PM-10:00 PM',
    'Saturday': '08:30 AM–12:00 PM',
    'Sunday': '08:30 AM–12:00 PM, 07:00 PM-10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Healthmark Pioneer Mall Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '638 Jurong West Street 61, Singapore 640638',

    'Hours': 
    {
    'Monday': '08:30 AM–01:30 PM, 04:30 PM-10:00 PM',
    'Tuesday': '08:30 AM–01:30 PM, 04:30 PM-10:00 PM',
    'Wednesday': '08:30 AM–01:30 PM, 04:30 PM-10:00 PM',
    'Thursday': '08:30 AM–01:30 PM, 04:30 PM-10:00 PM',
    'Friday': '08:30 AM–01:30 PM, 04:30 PM-10:00 PM',
    'Saturday': '08:30 AM–01:00 PM',
    'Sunday': '08:30 AM–01:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Wee Medical Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '378 Clementi Ave 5, Singapore 120378',

    'Hours': 
    {
    'Monday': '08:00 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Tuesday': '08:00 AM–03:00 PM',
    'Wednesday': '08:00 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Thursday': '08:00 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Friday': '08:00 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Saturday': '08:00 AM–03:00 PM',
    'Sunday': '08:00 AM–03:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'The Dublin Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '109 Clementi Street 11, #01-25, 120109',

    'Hours': 
    {
    'Monday': '08:00 AM–01:00 PM, 05:00 PM-08:00 PM',
    'Tuesday': '08:00 AM–01:00 PM, 05:00 PM-08:00 PM',
    'Wednesday': '08:00 AM–01:00 PM, 05:00 PM-08:00 PM',
    'Thursday': '08:00 AM–01:00 PM, 05:00 PM-08:00 PM',
    'Friday': '08:00 AM–01:00 PM',
    'Saturday': '10:00 AM–01:00 PM',
    'Sunday': '05:00 PM-08:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Healthway West Coast Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '727 Clementi West Street 2, #01-258, 120727',

    'Hours': 
    {
    'Monday': '08:30 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Tuesday': '08:30 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Wednesday': '08:30 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Thursday': '08:30 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Friday': '08:30 AM–03:00 PM, 06:00 PM-09:00 PM',
    'Saturday': '08:30 AM–12:30 PM',
    'Sunday': '08:30 AM–12:30 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'International Medical Clinic - Jelita',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': ' 293 Holland Rd, #02-04 Jelita Cold Storage Shopping Centre, 278628',

    'Hours': 
    {
    'Monday': '09:00 AM–05:30 PM',
    'Tuesday': '09:00 AM–05:30 PM',
    'Wednesday': '09:00 AM–05:30 PM',
    'Thursday': '09:00 AM–05:30 PM',
    'Friday': '09:00 AM–05:30 PM',
    'Saturday': '09:00 AM–01:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'C.M.C. Wong Binjai Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '15 Binjai Park, Singapore 589824',

    'Hours': 
    {
    'Monday': '09:00 AM–07:00 PM',
    'Tuesday': '09:00 AM–07:00 PM',
    'Wednesday': '09:00 AM–07:00 PM',
    'Thursday': '09:00 AM–07:00 PM',
    'Friday': '09:00 AM–07:00 PM',
    'Saturday': '09:00 AM–01:00 PM',
    'Sunday': '09:30 AM–11:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Macpherson Medical Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '78 Circuit Rd, #01-488, 370078',

    'Hours': 
    {
    'Monday': '09:00 AM–01:00 PM, 06:00 PM-08:30 PM',
    'Tuesday': '09:00 AM–01:00 PM, 06:00 PM-08:30 PM',
    'Wednesday': '09:00 AM–01:00 PM',
    'Thursday': '09:00 AM–01:00 PM, 06:00 PM-08:30 PM',
    'Friday': '09:00 AM–01:00 PM, 06:00 PM-08:30 PM',
    'Saturday': '09:00 AM–01:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Ansari Family Clinic & Surgery',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '78 Lor 25A Geylang, Singapore 388259',

    'Hours': 
    {
    'Monday': '08:00 AM–08:00 PM',
    'Tuesday': 'Closed',
    'Wednesday': '08:00 AM–08:00 PM',
    'Thursday': '08:00 AM–08:00 PM',
    'Friday': '08:00 AM–08:00 PM',
    'Saturday': '08:00 AM–02:00 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'SY Lee Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '400 Balestier Rd, 01- 11 Balestier Road, 329802',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM-04:30 PM',
    'Tuesday': '08:30 AM–12:30 PM, 02:00 PM-04:30 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM-04:30 PM',
    'Thursday': 'Closed',
    'Friday': '08:30 AM–12:30 PM, 02:00 PM-04:30 PM',
    'Saturday': '08:30 AM–12:30 PM, 02:00 PM-04:30 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Tham Clinic Singapore Pte.Ltd.',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '8 Whampoa Dr, Singapore 327718',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Tuesday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Thursday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Friday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Saturday': '08:30 AM–12:30 PM',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Health Partners Medical Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '23 Upper East Coast Rd, Singapore 455289',

    'Hours': 
    {
    'Monday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM, 07:00 PM-09:00 PM',
    'Tuesday': '08:30 AM–12:30 PM',
    'Wednesday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM, 07:00 PM-09:00 PM',
    'Thursday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Friday': '08:30 AM–12:30 PM, 02:00 PM-05:00 PM',
    'Saturday': '08:30 AM–12:30 PM',
    'Sunday': '08:30 AM–12:30 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Central 24-HR Clinic (Bedok)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': 'Blk 219 Bedok Central #01-124, 460219',

    'Hours': 
    {
      'Monday': 'Open 24 hours',
      'Tuesday': 'Open 24 hours',
      'Wednesday': 'Open 24 hours',
      'Thursday': 'Open 24 hours',
      'Friday': 'Open 24 hours',
      'Saturday': 'Open 24 hours',
      'Sunday': 'Open 24 hours',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'The Clinic @ Tai Seng',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '18 tai seng street, #01-07/08, 539775',

    'Hours': 
    {
    'Monday': '08:30 AM–01:00 PM, 02:00 PM-06:00 PM',
    'Tuesday': '08:30 AM–01:00 PM, 02:00 PM-06:00 PM',
    'Wednesday': '08:30 AM–01:00 PM, 02:00 PM-06:00 PM',
    'Thursday': '08:30 AM–01:00 PM, 02:00 PM-06:00 PM',
    'Friday': '08:30 AM–01:00 PM, 02:00 PM-06:00 PM',
    'Saturday': 'Closed',
    'Sunday': 'Closed',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'My Family Clinic (Toa Payoh Central)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '79D Toa Payoh Central #01-53, 314079',

    'Hours': 
    {
    'Monday': '08:30 AM–02:00 PM, 02:30 PM-04:30 PM, 06:00 PM-10:00 PM',
    'Tuesday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Wednesday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Thursday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Friday': '08:30 AM–02:00 PM, 06:00 PM-10:00 PM',
    'Saturday': '08:30 AM–02:00 PM',
    'Sunday': '09:00 AM-12:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Tay Family Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '1326 Serangoon Avenue 3, #01-376, 550326',

    'Hours': 
    {
    'Monday': '08:30 AM–01:00 PM, 06:30 PM-09:00 PM',
    'Tuesday': '08:30 AM–01:00 PM, 06:30 PM-09:00 PM',
    'Wednesday': '08:30 AM–01:00 PM, 06:30 PM-09:00 PM',
    'Thursday': '08:30 AM–01:00 PM, 06:30 PM-09:00 PM',
    'Friday': '08:30 AM–01:00 PM, 06:30 PM-09:00 PM',
    'Saturday': '08:30 AM–12:30 PM',
    'Sunday': '08:30 AM–12:30 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'OneCare Medical Clinic Hougang',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': 'Blk 104 Hougang Ave 1, #01-1125, 530104',

    'Hours': 
    {
    'Monday': '08:00 AM–03:00 PM, 05:00 PM-10:00 PM',
    'Tuesday': '08:00 AM–03:00 PM, 05:00 PM-10:00 PM',
    'Wednesday': '08:00 AM–03:00 PM, 05:00 PM-10:00 PM',
    'Thursday': '08:00 AM–03:00 PM, 05:00 PM-10:00 PM',
    'Friday': '08:00 AM–03:00 PM, 05:00 PM-10:00 PM',
    'Saturday': '08:00 AM–01:00 PM, 06:00 PM-10:00 PM',
    'Sunday': '08:00 AM–01:00 PM, 06:00 PM-10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'Kensington Family Clinic',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '14D Kensington Park Rd, Singapore 557265',

    'Hours': 
    {
    'Monday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    'Tuesday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    'Wednesday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    'Thursday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    'Friday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    'Saturday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    'Sunday': '08:00 AM–03:00 PM, 06:00 PM-10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'OneDoctors Medical Centre (nex)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '23 Serangoon Central, #01-54/55 NEX, Singapore 556083',

    'Hours': 
    {
    'Monday': '09:00 AM–09:00 PM',
    'Tuesday': '01:00 AM–06:00 PM',
    'Wednesday': '09:00 AM–09:00 PM',
    'Thursday': '09:00 AM–09:00 PM',
    'Friday': '09:00 AM–09:00 PM',
    'Saturday': '09:00 AM–09:00 PM',
    'Sunday': '09:00 AM–06:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
{
   'ClinicName':'My Family Clinic (Hougang Central)',
        
    'Category': 'GP',

    'Specialties': [
    'General Practice/Family Medicine',
    ],

    'Minimum Fee per session': 'none',
    'Maximum Fee per session': 'none',

    'Insurance': [],

    'Address': '804 Hougang Central, Singapore 530804',

    'Hours': 
    {
    'Monday': '08:00 AM–12:00 AM',
    'Tuesday': '08:00 AM–12:00 AM',
    'Wednesday': '08:00 AM–12:00 AM',
    'Thursday': '08:00 AM–12:00 AM',
    'Friday': '08:00 AM–12:00 AM',
    'Saturday': '08:00 AM–10:00 PM',
    'Sunday': '08:00 AM–10:00 PM',
    },
    'Doctor(s)/practitioner': 
    [],
},
]


