import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-accountsettings',
  templateUrl: './accountsettings.component.html',
  styleUrls: ['./accountsettings.component.css']
})
export class AccountsettingsComponent {
  editMode = false;
  recipeForm: FormGroup;

  constructor(
  ) { }

  // private initForm() {
  //   let recipeName = '';
  //   let recipeImagePath = '';
  //   let recipeDescription = '';
  //   let recipeIngredients = new FormArray([]);

  //   if (this.editMode) {

  //     // const recipe = this.recipeService.getRecipe(this.id);

  //     recipeName = recipe.name;
  //     recipeImagePath = recipe.imagePath;
  //     recipeDescription = recipe.description;

  //     if (recipe['ingredients']) {
  //       for (const ingredient of recipe.ingredients) {
  //         recipeIngredients.push(
  //           new FormGroup({
  //             'name': new FormControl(ingredient.name, Validators.required),
  //             'amount': new FormControl(ingredient.amount, [
  //               Validators.required,
  //               Validators.pattern(/^[1-9]+[0-9]*$/)
  //             ])
  //           })
  //         );
  //       }
  //     }
  //   }

  //   this.recipeForm = new FormGroup({
  //     'name': new FormControl(recipeName, Validators.required),
  //     'imagePath': new FormControl(recipeImagePath, Validators.required),
  //     'description': new FormControl(recipeDescription, Validators.required),
  //     'ingredients': recipeIngredients
  //   })
  // }

}
