import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountSettingsModule } from './accountsettings.module';
import { AccountsettingsComponent } from './accountsettings.component';
import { ProfileComponent } from './profile/profile.component';
import { MyClinicsComponent } from './my-clinics/my-clinics.component';
import { AccountClinicComponent } from './my-clinics/account-clinic/account-clinic.component';
import { AccountClinicEditComponent } from './my-clinics/account-clinic-edit/account-clinic-edit.component';

const AccountSettingsRoutes: Routes = [
    {path: '', component: AccountsettingsComponent, children: [
        {
          path: 'profile',
          component: ProfileComponent
        },
        {
          path: 'my-clinics',
          component: MyClinicsComponent,
          children: [
            { path: 'new', component: AccountClinicEditComponent },
            { path: ':id', component: AccountClinicComponent },
            { path: ':id/edit', component: AccountClinicEditComponent },
          ]
        },
        {
          path: '',
          redirectTo: 'profile',
          pathMatch: 'full'
        },
    ]}
]

@NgModule({
    imports: [
        RouterModule.forChild(AccountSettingsRoutes)
    ],
    exports: [RouterModule],
    providers: [ ]
})

export class AccountSettingsRoutingModule {}