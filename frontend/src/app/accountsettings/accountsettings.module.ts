import { NgModule } from '@angular/core'
import { AccountsettingsComponent } from './accountsettings.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountSettingsRoutingModule } from './accountsettings-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { MyClinicsComponent } from './my-clinics/my-clinics.component';
import { ClinicListComponent } from './my-clinics/clinic-list/clinic-list.component';
import { ClinicListItemComponent } from './my-clinics/clinic-list/clinic-list-item/clinic-list-item.component';
import { AccountClinicComponent } from './my-clinics/account-clinic/account-clinic.component';
import { AccountClinicEditComponent } from './my-clinics/account-clinic-edit/account-clinic-edit.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations:[
        AccountsettingsComponent,
        ClinicListComponent,
        ProfileComponent,
        MyClinicsComponent,
        ClinicListItemComponent,
        AccountClinicComponent,
        AccountClinicEditComponent
    ],
    imports:[
        HttpClientModule,
        CommonModule,
        ReactiveFormsModule,
        AccountSettingsRoutingModule
    ]
})

export class AccountSettingsModule {}